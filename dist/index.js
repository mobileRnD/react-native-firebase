import firebase from './modules/core/firebase';
export default firebase;
export * from './modules/core/firebase';

export const NotificationCategoryOption = {
  CustomDismissAction: 'UNNotificationCategoryOptionCustomDismissAction',
  AllowInCarPlay: 'UNNotificationCategoryOptionAllowInCarPlay',
  PreviewsShowTitle: 'UNNotificationCategoryOptionHiddenPreviewsShowTitle',
  PreviewsShowSubtitle: 'UNNotificationCategoryOptionHiddenPreviewsShowSubtitle',
  None: 'UNNotificationCategoryOptionNone'
};

export const NotificationActionOption = {
  AuthenticationRequired: 'UNNotificationActionOptionAuthenticationRequired',
  Destructive: 'UNNotificationActionOptionDestructive',
  Foreground: 'UNNotificationActionOptionForeground',
  None: 'UNNotificationActionOptionNone',
};

export const NotificationActionType = {
  Default: 'UNNotificationActionTypeDefault',
  TextInput: 'UNNotificationActionTypeTextInput',
};
/*
 * Export App types
 */
